<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, 

## Installation
- clone this repo
- run ````composer install````
- run ````php artisan key:generate```` if the key is not generated by self
- run ````php artisan migrate:fresh --seed```` to migrate and seeding default data
- run ````php artisan laravol:indonesia:Seed```` to seeding region service database

## Package 
| Package Name | Link | Description |
| --- | --- | --- |
| `Laravel Nova resource` | <a href="https://nova.laravel.com/">Laravel Nova</a> | Nova is a beautifully designed administration panel for Laravel. Carefully crafted by the creators of Laravel to make you the most productive developer in the galaxy. |
| `eminart/Nova-Permissions` | <a href="https://github.com/eminiarts/nova-permissions">Nova Permissions</a> | A Laravel Nova Tool that allows you to group your Permissions into Groups and attach it to Users. It uses Spatie's laravel-permission. |
|`Laravel Spatie/Role-Permissions`| <a href="https://spatie.be/docs/laravel-permission/v4/introduction">Spatie/Permissions</a> | This package allows you to manage user permissions and roles in a database. |
| Laravolt Indeonesi Region | <a href="https://github.com/laravolt/indonesia">Laravolt</a> | Package Laravel yang berisi data Provinsi, Kabupaten/Kota, dan Kecamatan/Desa di seluruh Indonesia. |



# Thanks For :
Great Thanks For this Owner of :
- laravel Nova resource <a href="https://nova.laravel.com/">Laravel Nova</a>
- eminart/Nova-Permissions <a href="https://github.com/eminiarts/nova-permissions">Nova Permissions</a>
- Laravel Spatie/Role-Permissions <a href="hhttps://spatie.be/docs/laravel-permission/v4/introduction">Spatie/Permissions</a>
- Laravolt?indonesia <a href="https://github.com/laravolt/indonesia">Laravolt/Indonesia</a>

## Change Log
- [x] Setting Up Nova Installation & Configuration [done]
- [x] Settting Laravel Nova Role / Permissions Policies [done]
- [x] Setting Database Seeder for default permissions Seeder [done]
- [x] Setting up Laravolt/Indonesia Package [done] 
