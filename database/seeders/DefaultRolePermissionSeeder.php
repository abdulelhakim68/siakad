<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Spatie\Permission\PermissionRegistrar;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use Illuminate\Support\Str;

class DefaultRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\spatie\permission\PermissionRegistrar::class]->forgetCachedPermissions();
        $collection = collect([
            'users',
            'roles',
            'permissions'
        ]);
        $collection->each(function ($item, $key) {
            Permission::create(['group' => $item, 'name' => 'viewAny' . " - " . $item]);
            Permission::create(['group' => $item, 'name' => 'view' . " - " . $item]);
            Permission::create(['group' => $item, 'name' => 'create' . " - " . $item]);
            Permission::create(['group' => $item, 'name' => 'update' . " - " . $item]);
            Permission::create(['group' => $item, 'name' => 'delete' . " - " . $item]);
            Permission::create(['group' => $item, 'name' => 'restore' . " - " . $item]);
            Permission::create(['group' => $item, 'name' => 'forceDelete' . " - " . $item]);
        });


        // create role
        $role = Role::create(['name' => 'super-admin']);

        // create example user 
        $user = \App\Models\User::factory()->create([
            'name'              => 'super-admin',
            'email'             => 'superadmin@example.com',
            'email_verified_at' => now(),
            'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token'    => Str::random(10),
        ]);
        $user->assignRole($role);
    }
}
